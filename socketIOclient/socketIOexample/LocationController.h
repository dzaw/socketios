//
//  LocationController.h
//  socketIOexample
//
//  Created by dagmara on 15/05/2018.
//  Copyright © 2018 saturngod. All rights reserved.
//

#ifndef LocationController_h
#define LocationController_h

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationController : UIViewController <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
}

@end

#endif /* LocationController_h */
