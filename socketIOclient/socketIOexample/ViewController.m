//
//  ViewController.m
//  socketIOexample
//
//  Created by Htain Lin Shwe on 21/10/13.
//  Copyright (c) 2013 saturngod. All rights reserved.
//

#import "ViewController.h"
#import "SocketIOPacket.h"
#import "LocationController.h"

@interface ViewController ()

@end

@implementation ViewController

CLLocationManager *locationManager;
//NSString *deviceName = @"iOS";

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _socketIO = [[SocketIO alloc] initWithDelegate:self];
    //[_socketIO connectToHost:@"192.168.119.143" onPort:3000];
    //[_socketIO connectToHost:@"192.168.119.143" onPort:42474 withParams:@{@"model":@"iPhone", @"manf":@"Apple", @"id":@"iphonetesting", @"release":@"9_0"}];
    [_socketIO connectToHost:@"192.168.1.5" onPort:42474 withParams:@{@"model":@"iPhone", @"manf":@"Apple", @"id":@"iphonetesting", @"release":@"9_0"}];
    //[_socketIO connectToHost:@"185.180.206.220" onPort:42474 withParams:@{@"model":@"iPhone", @"manf":@"Apple", @"id":@"iphonetesting", @"release":@"9_0"}];
    
    [_socketIO sendEvent:@"join" withData:@"iOS"];
    [_socketIO sendMessage:@"ping"];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        //[locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization]; //ask for permissions first time
    //[locationManager startUpdatingLocation];
    
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"Location %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    NSString *locmessage = [NSString stringWithFormat:@"Location %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude];
    [self addNewEventWithNickName:@"iOS" AndMessage:locmessage]; //add location to chatbox view
    [_socketIO sendMessage:locmessage]; //send message with location to server
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation0:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sendMsg:(id)sender {
    [_socketIO sendEvent:@"message" withData:_chatbox.text];
    [self addNewEventWithNickName:@"iOS" AndMessage:_chatbox.text];
    _chatbox.text = @"";
    
}

-(void)addNewEventWithNickName:(NSString*)nickname AndMessage:(NSString*)message
{
    _chatLog.text = [_chatLog.text stringByAppendingFormat:@"%@ - %@\n",nickname,message];
    
    if ([message  isEqual: @"PING"]){
        NSLog(message);
        [_socketIO sendEvent:@"message" withData:@"PONG"];
    }
    
    if ([message  isEqual: @"GEOSTART"]){
        NSLog(message);
        [locationManager startUpdatingLocation];
    }
    
    if ([message  isEqual: @"GEOSTOP"]){
        NSLog(message);
        [locationManager stopUpdatingLocation];
    }
    
    
}
# pragma mark -
# pragma mark socket.IO-objc delegate methods

- (void) socketIODidConnect:(SocketIO *)socket
{
    NSLog(@"socket.io connected.");
}

- (void) socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet
{
    NSLog(@"didReceiveEvent()");
    
    if([packet.name isEqualToString:@"message"])
    {
        NSArray* args = packet.args;
        NSDictionary* arg = args[0];
        
        [self addNewEventWithNickName:arg[@"nickname"] AndMessage:arg[@"message"]];
        
    }
}

- (void) socketIO:(SocketIO *)socket onError:(NSError *)error
{
    NSLog(@"onError() %@", error);
}


- (void) socketIODidDisconnect:(SocketIO *)socket disconnectedWithError:(NSError *)error
{
    NSLog(@"socket.io disconnected. did error occur? %@", error);
    [locationManager stopUpdatingLocation];
    
    //[_socketIO connectToHost:@"192.168.119.143" onPort:42474 withParams:@{@"testing":@"iPhone"}];
    //[_socketIO connectToHost:@"185.180.206.220" onPort:42474 withParams:@{@"model":@"iPhone", @"manf":@"Apple", @"id":@"iphonetesting", @"release":@"9_0"}];
    [_socketIO sendEvent:@"join" withData:@"iOS"];
    [_socketIO sendMessage:@"ping reconnect"];
}


@end
